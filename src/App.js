import "./App.css";
import Blog from "./components/blog/Blog";
import Nav from "./components/navbar/Nav";
import Product from "./components/product/Product";
import Question from "./components/question/Question";
import { Routes, Route } from "react-router-dom";

function App() {
  return (
    <div className="App">
      <Nav />
      <Routes>
        <Route path="/" element={<Product />} />
        <Route path="/question" element={<Question />} />
        <Route path="/blog" element={<Blog />} />
      </Routes>
    </div>
  );
}

export default App;
