import React, { useState, useEffect } from "react";
import axios from "axios";

import "./blog.css";

function Blog() {
  const [state, setState] = useState([]);

  useEffect(() => {
    axios.get("/stories/").then((res) => {
      const persons = res.data;
      setState(persons);
      console.log(state);
    });
  }, []);
  return (
    <div className="blog">
      {state.map((item) => {
        return (
          <ul key={item.id}>
            <li>
              <img src={item.image} alt="img" />
            </li>
            <li>{item.name}</li>
          </ul>
        );
      })}
    </div>
  );
}

export default Blog;
