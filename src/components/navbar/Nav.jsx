import React from "react";
import { Link } from "react-router-dom";

import "./nav.css"

function Nav() {
  return (
    <div className="navbar">
      <ul>
        <li>
          <Link to="/">Product</Link>
        </li>
        <li>
          <Link to="/question"> Question</Link>
        </li>
        <li>
          <Link to="/blog">Blog</Link>
        </li>
      </ul>
    </div>
  );
}

export default Nav;
