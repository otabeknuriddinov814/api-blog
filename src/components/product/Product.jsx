import React, { useState, useEffect } from "react";
import axios from "axios";

import "./product.css";

function Product() {
  const [state, setState] = useState([]);

  useEffect(() => {
    axios.get("/products/").then((res) => {
      console.log(res.data);
      const persons = res.data;
      setState(persons);
      console.log(state);
    });
  }, []);
  return (
    <div className="product">
      {state.map((item) => {
        return (
          <ul key={item.id}>
            <li>
              <img src={item.image} alt="img" />
            </li>
            <li>{item.name}</li>
          </ul>
        );
      })}
    </div>
  );
}

export default Product;
