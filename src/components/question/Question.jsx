import React, { useEffect, useState } from "react";
import axios from "axios";

import "./question.css";

function Question() {
  const [state, setState] = useState([]);

  useEffect(() => {
    axios.get("/questions/").then((res) => {
      const persons = res.data;
      setState(persons);
      console.log(state);
    });
  }, []);

  return (
    <div className="question">
      {state.map((item) => {
        return (
          <ul key={item.id}>
            <li>{item.question}</li>
            <li>{item.answer}</li>
          </ul>
        );
      })}
    </div>
  );
}

export default Question;
